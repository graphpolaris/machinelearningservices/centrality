##
# This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
# © Copyright Utrecht University (Department of Information and Computing Sciences)
##

# !/usr/bin/env python
import os
import sys
import inspect
import networkx as nx
import json


from machine_learning_common.MLBasicFunctions import addNodeMetaData, buildGraph
from machine_learning_common.MLRepositoryInterface import MLServerInterface

# We often compare against this specific string, so we clearly define it to prevent any typos
# Python has no constants so we simply give it a very obvious name that implies it is not supposed to be changed
ML_QUEUE_NAME = "ctr_queue"
SERVICE_NAME = "centrality"


##
# MLServer implements the MLServerInterface interface
##
class MLServer(MLServerInterface):
    ##
    # __init__ initialises the MLServer with the proper parameters
    #   self: Self@MLServer, the MLServer implementation
    #   Return: None, return nothing
    ##
    def __init__(self):
        # Fill in the parameters for communication with the algorithm here
        super().__init__(SERVICE_NAME, ML_QUEUE_NAME)

    ##
    # decodeMessage builds a NetworkX graph based on the incoming query data
    #   self: Self@MLServer, the MLServer implementation
    #   incomingQueryData: Any, the incoming query data in JSON format
    #   Return: Graph, the NetworkX graph
    ##
    def decodeMessage(self, incomingQueryData):
        graph = buildGraph(incomingQueryData)
        return graph

    ##
    # __call__ takes an incoming message and applies the machine learning algorithm and data transformations
    #   self: Self@MLServer, the MLServer implementation
    #   body: Any, the body of the incoming RabbitMQ message
    #   Return: str, a formatted JSON string of the query result after the application of a machine learning algorithm
    ##
    def __call__(self, body):
        # Decode the incoming RabbitMQ message
        incomingQueryData = json.loads(body.decode())
        # Log the queryID and type
        print(incomingQueryData["queryID"])
        print(incomingQueryData["type"])
        # Decode the incoming query data into a NetworkX graph
        G = self.decodeMessage(incomingQueryData)
        # This is where the specific algorithm is actually called
        mlresult = nx.degree_centrality(G)
        # Centrality results are added to nodes
        # Transforms the result (a dictionary) into JSON
        # print(mlresult)

        return mlresult
