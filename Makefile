docker: login
	@docker build -t harbor.graphpolaris.com/graphpolaris/machine-learning-ctr-service:latest .

login:
	echo -e "machine git.science.uu.nl\nlogin gitlab-ci-token\npassword ${CI_JOB_TOKEN}" > ~/.netrc

develop:
	poetry run python Centrality/wrapper.py